import Image, { StaticImageData } from 'next/image';
import ListItemTitle from './ListItemTitle';
import SectionTitle from './SectionTitle';
import { css } from '@stitches/react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Pagination } from 'swiper/modules';

const classDescription = css({
  fontSize: '$m3',
  marginTop: '20px',
  marginBottom: '69px',
});

const classSwiperContainer = css({
  ['--swiper-theme-color']: '#5C3CAF',
  ['--swiper-pagination-bullet-inactive-color']: '#D8D8D8',
  ['--swiper-pagination-bullet-inactive-opacity']: 1,
  ['--swiper-pagination-bullet-size']: '10px',
  ['--swiper-pagination-bottom']: '30px',
  ['--swiper-pagination-bullet-horizontal-gap']: '6px',
  gridArea: '3 / 1 / 5 / 2',
});

type ListItem = {
  num: string;
  title: string;
  description: string;
};

type Props = {
  title: string;
  image: {
    src: StaticImageData;
    marginTop: number;
    marginRight: number;
    marginBottom: number;
    marginLeft: number;
    overlappingArea: number;
  };
  alt: string;
  listItems: [ListItem, ListItem, ListItem];
};

const Section: React.FC<Props> = ({ title, image, alt, listItems }) => {
  const classContainer = css({
    display: 'grid',
    gridTemplateColumns: '100%',
    gridTemplateRows: `auto auto ${image.overlappingArea}px auto`,
  });

  const classImageContainer = css({
    marginTop: image.marginTop,
    gridArea: '2 / 1 / 4 / 2',
    zIndex: 2,
  });

  const classTextContainer = css({
    background: '$midgray',
    paddingTop: `${image.marginBottom}px`,
    paddingLeft: '$paddingX',
    paddingRight: '$paddingX',
    height: '17.75rem',
  });

  return (
    <section className={classContainer()}>
      <SectionTitle css={{ paddingLeft: '$paddingX' }}>{title}</SectionTitle>
      <div className={classImageContainer()}>
        <Image
          src={image.src}
          alt={alt}
          style={{
            maxWidth: `calc(100% - ${image.marginLeft + image.marginRight}px)`,
            height: 'auto',
            marginLeft: image.marginLeft,
            marginRight: image.marginRight,
          }}
        />
      </div>
      <div className={classSwiperContainer()}>
        <Swiper modules={[Pagination]} pagination={{ clickable: true }}>
          {listItems.map((listItem) => (
            <SwiperSlide key={listItem.num}>
              <div className={classTextContainer()}>
                <ListItemTitle num={listItem.num} title={listItem.title} />
                <p className={classDescription()}>{listItem.description}</p>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </section>
  );
};

export default Section;
