import { styled } from '@stitches/react';

const SectionTitle = styled('h1', {
  fontWeight: 'normal',
  fontSize: '$m1',
  color: '$lightgray',
});

SectionTitle.displayName = 'SectionTitle';

export default SectionTitle;
