import { css } from '@stitches/react';

const classContainer = css({
  display: 'flex',
  alignItems: 'center',
  gap: '0.63rem',
});

const classNumberContainer = css({
  display: 'flex',
  flexDirection: 'column',
  gap: '0.125rem',
});

const classNumber = css({
  fontSize: '$m4',
  textAlign: 'center',
  letterSpacing: '0.09375rem',
});

const classUnderline = css({
  width: '1.0625rem',
  height: '0.25rem',
  borderRadius: '0.15625rem',
  background: '$purple',
});

const classTitle = css({
  fontSize: '$m2',
  color: '$gray',
  letterSpacing: '0.09375rem',
});

type Props = {
  num: string;
  title: string;
};

const ListItemTitle: React.FC<Props> = ({ num, title }) => {
  return (
    <div className={classContainer()}>
      <div className={classNumberContainer()}>
        <div className={classNumber()}>{num}</div>
        <div className={classUnderline()} />
      </div>
      <div className={classTitle()}>{title}</div>
    </div>
  );
};

export default ListItemTitle;
