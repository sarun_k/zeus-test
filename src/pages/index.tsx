import Section from '../components/Section';
import basketballerImage from '../../public/images/basketball-m.png';
import footballerImage from '../../public/images/football-m.png';
import { css } from '@stitches/react';

const classPage = css({
  display: 'flex',
  flexDirection: 'column',
  minWidth: '18.75rem',
  gap: '19px',
});

export default function Home() {
  return (
    <div className={classPage()}>
      <Section
        title="ATHLETS"
        image={{
          src: footballerImage,
          marginTop: 11,
          marginRight: 50,
          marginBottom: 72,
          marginLeft: 52,
          overlappingArea: 55,
        }}
        alt="footballer"
        listItems={[
          {
            num: '01',
            title: 'CONNECTION',
            description:
              'Connect with coaches directly, you can ping coaches to view profile.',
          },
          {
            num: '02',
            title: 'COLLABORATION',
            description:
              'Work with other student athletes to increase visability. When you share and like other players videos it will increase your visability as a player. This is the team work aspect to Surface 1.',
          },
          {
            num: '03',
            title: 'GROWTH',
            description:
              'Resources and tools for you to get better as a student Athelte. Access to training classes, tutor sessions, etc',
          },
        ]}
      />
      <Section
        title="PLAYERS"
        image={{
          src: basketballerImage,
          marginTop: 22,
          marginRight: 15,
          marginBottom: 72,
          marginLeft: 3,
          overlappingArea: 35,
        }}
        alt="basketballer"
        listItems={[
          {
            num: '01',
            title: 'CONNECTION',
            description:
              'Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.',
          },
          {
            num: '02',
            title: 'COLLABORATION',
            description:
              'Work with recruiter to increase your chances of finding talented athlete.',
          },
          {
            num: '03',
            title: 'GROWTH',
            description:
              'Save your time, recruit proper athlets for your team.',
          },
        ]}
      />
    </div>
  );
}
