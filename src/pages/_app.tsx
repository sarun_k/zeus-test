import { globalCss } from '@stitches/react';
import { AppProps } from 'next/app';
import { Roboto } from 'next/font/google';

const roboto = Roboto({ weight: '400', subsets: ['latin'] });

const globalStyles = globalCss({
  '*': { boxSizing: 'border-box', margin: 0, padding: 0 },
});

export default function MyApp({ Component, pageProps }: AppProps) {
  globalStyles();
  return (
    <main className={roboto.className}>
      <Component {...pageProps} />
    </main>
  );
}
