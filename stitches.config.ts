import { createStitches } from '@stitches/react';

export const { styled, getCssText } = createStitches({
  theme: {
    fontSizes: {
      d1: '5.625rem',
      d2: '2.25rem',
      d3: '1.25rem',
      d4: '1.125rem',
      m1: '3.125rem',
      m2: '1.75rem',
      m3: '0.9375rem',
      m4: '0.875rem',
    },
    colors: {
      gray: '#C2C2C2',
      midgray: '#F5F4F9;',
      lightgray: '#E7E7E7',
      purple: '#603EBE',
    },
    space: {
      paddingX: '18px',
    },
  },
});
